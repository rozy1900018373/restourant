package com.company;
import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Menu minuman = new Minuman();
        Menu makanan = new Makanan();
        Harga harga = new Harga();
        Member[] member = new Member[10];
        member[0] = new Member(1111);
        Scanner input = new Scanner(System.in);
        System.out.println("Selamat Datang");
        System.out.println("1. Member ");
        System.out.println("2. Guest");
        System.out.println("3. Admin");
        try (FileWriter f = new FileWriter("transaksi.txt", true);
             BufferedWriter b = new BufferedWriter(f);
             PrintWriter p = new PrintWriter(b)) {
            System.out.print("Masuk Sebagai : ");
            int masuk = input.nextInt();
            if (masuk == 1) {
                try {
                    System.out.print("Masukkan ID anda : ");
                    int id = input.nextInt();
                    for (int i = 0; i < 10; i++) {
                        if (member[i].getID() == id) {
                            System.out.println("1. Makanan");
                            System.out.println("2. Minuman");
                            System.out.print("Masukkan Pilihan Anda :");
                            int pil = input.nextInt();
                            if (pil == 1){
                                makanan.daftarMenu();
                                System.out.print("Masukkan Pilihan Anda : ");
                                int pil2 = input.nextInt();
                                if (pil2 == 1){
                                    System.out.println("Get Diskon 30 %");
                                    System.out.println("Total Harga : " +harga.getDisk1());
                                    p.println("Pesanan Member");
                                    p.println("ID Member : " + member[i].getID());
                                    p.println("Nama Pesanan : Nasi Goreng");
                                    p.println("Harga : " + harga.getDisk1());
                                    p.println("===============================");
                                }else if (pil2 == 2){
                                    System.out.println("Get Diskon 30 %");
                                    System.out.println("Total Harga : " + harga.getDisk2());
                                    p.println("Pesanan Member");
                                    p.println("ID Member : " + member[i].getID());
                                    p.println("Nama Pesanan : Mie");
                                    p.println("Harga : " + harga.getDisk2());
                                    p.println("===============================");
                                }else if (pil2 == 3){
                                    System.out.println("Get Diskon 30 %");
                                    System.out.println("Total Harga : " + harga.getDisk3());
                                    p.println("Pesanan Member");
                                    p.println("ID Member : " + member[i].getID());
                                    p.println("Nama Pesanan : Kentang");
                                    p.println("Harga : " + harga.getDisk3());
                                    p.println("===============================");
                                }else{
                                    throw new Exception("Input Data Tidak Sesuai");
                                }
                            }else if (pil == 2){
                                minuman.daftarMenu();
                                System.out.print("Masukkan Pilihan Anda : ");
                                int pil2 = input.nextInt();
                                if (pil2 == 1){
                                    System.out.println("Get Diskon 30 %");
                                    System.out.println("Total Harga : " + harga.getDisk4());
                                    p.println("Pesanan Member");
                                    p.println("ID Member : " + member[i].getID());
                                    p.println("Nama Pesanan : Jus Anggur");
                                    p.println("Harga : " + harga.getDisk4());
                                    p.println("===============================");
                                }else if (pil2 == 2){
                                    System.out.println("Get Diskon 30 %");
                                    System.out.println("Total Harga : " + harga.getDisk5());
                                    p.println("Pesanan Member");
                                    p.println("ID Member : " + member[i].getID());
                                    p.println("Nama Pesanan : Jus Alpukat");
                                    p.println("Harga : " + harga.getDisk5());
                                    p.println("===============================");
                                }else if (pil2 == 3){
                                    System.out.println("Get Diskon 30 %");
                                    System.out.println("Total Harga : " + harga.getDisk6());
                                    p.println("Pesanan Member");
                                    p.println("ID Member : " + member[i].getID());
                                    p.println("Nama Pesanan : Jus Mangga");
                                    p.println("Harga : " + harga.getDisk6());
                                    p.println("===============================");
                                }else{
                                    throw new Exception("Input Data Tidak Sesuai");
                                }
                            }else{
                                throw new Exception("Input Data Tidak Sesuai");
                            }
                            break;
                        }
                    }
                } catch (NullPointerException r) {
                    System.out.println("ID Tidak Ditemukan.");
                }

            } else if (masuk == 2) {
                System.out.println("1. Makanan");
                System.out.println("2. Minuman");
                System.out.print("Masukkan Pilihan Anda : ");
                int pil = input.nextInt();
                if (pil == 1){
                    makanan.daftarMenu();
                    System.out.print("Masukkan Pilihan Anda : ");
                    int pil2 = input.nextInt();
                    if (pil2 == 1){
                        System.out.println("Total Harga : " + harga.makanan1);
                        p.println("Pesanan Guest");
                        p.println("Nama Pesanan : Nasi Goreng");
                        p.println("Harga : " + harga.makanan1);
                        p.println("===============================");
                    }else if (pil2 == 2){
                        System.out.println("Total Harga : " + harga.makanan2);
                        p.println("Pesanan Guest");
                        p.println("Nama Pesanan : Mie");
                        p.println("Harga : " + harga.makanan2);
                        p.println("===============================");
                    }else if (pil2 == 3){
                        System.out.println("Total Harga : " + harga.makanan3);
                        p.println("Pesanan Guest");
                        p.println("Nama Pesanan : Kentang");
                        p.println("Harga : " + harga.makanan3);
                        p.println("===============================");
                    }else{
                        throw new Exception("Input Data Tidak Sesuai");
                    }
                }else if (pil == 2){
                    minuman.daftarMenu();
                    System.out.print("Masukkan Pilihan Anda : ");
                    int pil2 = input.nextInt();
                    if (pil2 == 1){
                        System.out.println("Total Harga : " + harga.minuman1);
                        p.println("Pesanan Guest");
                        p.println("Nama Pesanan : Jus Anggur");
                        p.println("Harga : " + harga.minuman1);
                        p.println("===============================");
                    }else if (pil2 == 2){
                        System.out.println("Total Harga : " + harga.minuman2);
                        p.println("Pesanan Guest");
                        p.println("Nama Pesanan : Jus Alpukat");
                        p.println("Harga : " + harga.minuman2);
                        p.println("===============================");
                    }else if (pil2 == 3){
                        System.out.println("Total Harga : " + harga.minuman3);
                        p.println("Pesanan Guest");
                        p.println("Nama Pesanan : Jus Mangga");
                        p.println("Harga : " + harga.minuman3);
                        p.println("===============================");
                    }else{
                        throw new Exception("Input Data Tidak Sesuai");
                    }
                }else{
                    throw new Exception("Input Data Tidak Sesuai");
                }

            } else if (masuk == 3) {
                System.out.print("Masukkan ID Admin : "); // 9999 ID ADMIN
                int a = input.nextInt();
                if (a == 9999){
                    System.out.println("-====== Data Transaksi ======-");
                    String fileName = "transaksi.txt" ;

                    try {
                        // membaca file
                        File myFile = new File(fileName);
                        Scanner fileReader = new Scanner(myFile);

                        // cetak isi file
                        while(fileReader.hasNextLine()){
                            String data = fileReader.nextLine();
                            System.out.println(data);
                        }

                    } catch (FileNotFoundException e) {
                        System.out.println("Terjadi Kesalahan: " + e.getMessage());
                        e.printStackTrace();
                    }
                }else{
                    throw new Exception("ID Admin Tidak Ditemukan");
                }


                //batas
            } else {
                throw new Exception("Input Data Tidak Sesuai");
            }
        }catch(Exception z){
            z.printStackTrace();
        }
    }
}